package ru.sut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoaLhsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoaLhsApplication.class, args);
	}
}
