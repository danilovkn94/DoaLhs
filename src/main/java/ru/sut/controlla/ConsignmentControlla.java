package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sut.model.tables.Consignment;
import ru.sut.model.tables.Manufacture;
import ru.sut.model.tables.Product;
import ru.sut.repository.CompanyInfoRepository;
import ru.sut.repository.ConsignmentRepository;
import ru.sut.repository.ManufactureRepository;
import ru.sut.repository.ProductRepository;

@Controller
public class ConsignmentControlla {

    private ProductRepository productRepository;
    private ManufactureRepository manufactureRepository;
    private ConsignmentRepository consignmentRepository;
    private final CompanyInfoRepository companyInfoRepository;

    @Autowired
    public ConsignmentControlla(ProductRepository productRepository, ManufactureRepository manufactureRepository,
                                ConsignmentRepository consignmentRepository, CompanyInfoRepository companyInfoRepository)
    {
        this.productRepository = productRepository;
        this.manufactureRepository = manufactureRepository;
        this.consignmentRepository = consignmentRepository;
        this.companyInfoRepository = companyInfoRepository;
    }

    @RequestMapping(value = "/consignment", method = RequestMethod.GET)
    public String consignment(Model model){

        model.addAttribute("preffix", companyInfoRepository.findAll().get(0).getPreffix());

        model.addAttribute("prods", productRepository.findAll());
        model.addAttribute("mans", manufactureRepository.findAll());
        model.addAttribute("cons", consignmentRepository.findAll());

        return "consignment";
    }

    @RequestMapping(value = "/saveConsignment", method = RequestMethod.POST)
    public String saveConsignment(@RequestParam("manufactureDate") String manufactureDate,
                                  @RequestParam("product") Product product,
                                  @RequestParam("manufacture")Manufacture manufacture)
    {
        Consignment consignment = new Consignment(manufactureDate, product, manufacture);
        consignmentRepository.saveAndFlush(consignment);

        return "redirect:/consignment";
    }

    @RequestMapping(value = "deleteConsignment", method = RequestMethod.POST)
    public String deleteConsignment(@RequestParam("id") int id){

        consignmentRepository.delete(id);

        return "redirect:/consignment";
    }
}
