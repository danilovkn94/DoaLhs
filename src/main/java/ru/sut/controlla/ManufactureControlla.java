package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sut.model.tables.Manufacture;
import ru.sut.repository.ManufactureRepository;

@Controller
public class ManufactureControlla {

    private final ManufactureRepository manufactureRepository;


    @Autowired
    public ManufactureControlla(ManufactureRepository manufactureRepository) {
        this.manufactureRepository = manufactureRepository;
    }

    @RequestMapping(value = "/manufactures", method = RequestMethod.GET)
    public String manufactures(Model model){

        model.addAttribute("mans", manufactureRepository.findAll());

        return "manufactures";
    }

    @RequestMapping(value = "/saveManufacture", method = RequestMethod.POST)
    public String saveManufacture(@RequestParam("name") String name, @RequestParam("address") String address){

        Manufacture manufacture = new Manufacture(name, address);
        manufactureRepository.saveAndFlush(manufacture);

        return "redirect:/manufactures";
    }

    @RequestMapping(value = "/deleteManufacture", method = RequestMethod.POST)
    public String deleteManufacture(@RequestParam("id") int id){

        manufactureRepository.delete(id);

        return "redirect:/manufactures";
    }
}
