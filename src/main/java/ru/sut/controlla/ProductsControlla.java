package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.sut.model.tables.Product;
import ru.sut.repository.ProductRepository;

import java.io.IOException;

@Controller
public class ProductsControlla {

    private final ProductRepository productRepository;

    @Autowired
    public ProductsControlla(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String products(Model model){

        model.addAttribute("products", productRepository.findAll());

        return "products";
    }

    @RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
    public String saveProduct(@RequestParam("name") String name, @RequestParam("info") String info,
                              @RequestParam("photo") MultipartFile photo) throws IOException {

        Product product = new Product(name, info, photo.getBytes());
        productRepository.saveAndFlush(product);


        return "redirect:/products";
    }

    @RequestMapping(value = "/deleteProduct", method = RequestMethod.POST)
    public String deleteProduct(@RequestParam("id") int id){

        productRepository.delete(id);

        return "redirect:/products";
    }
}
