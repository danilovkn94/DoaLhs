package ru.sut.controlla;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class QrCodeControlla {

    @RequestMapping(value = "/getQrcode", method = RequestMethod.POST)
    public String getQrCode(Model model, @RequestParam("prefix") String prefix,
                            @RequestParam("suffix") String suffix)
    {
        model.addAttribute("doi", prefix + "/" + suffix);

        return "qrcode";
    }
}
