package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sut.model.tables.Consignment;
import ru.sut.repository.ConsignmentRepository;

@RestController
@RequestMapping("/rest")
public class RestControlla {

    private final ConsignmentRepository consignmentRepository;

    @Autowired
    public RestControlla(ConsignmentRepository consignmentRepository) {
        this.consignmentRepository = consignmentRepository;
    }

    @GetMapping("/{suffix}/")
    public Consignment getInfo( @PathVariable String suffix){

        String[] s = suffix.split("\\.");

        return consignmentRepository.findOne(Integer.parseInt(s[s.length - 1]));
    }
}
