package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sut.model.identificator.Doi;
import ru.sut.repository.CompanyInfoRepository;
import ru.sut.repository.ConsignmentRepository;

@Controller
public class SearchControlla {

    private final ConsignmentRepository consignmentRepository;
    private final CompanyInfoRepository companyInfoRepository;

    @Autowired
    public SearchControlla(ConsignmentRepository consignmentRepository, CompanyInfoRepository companyInfoRepository) {
        this.consignmentRepository = consignmentRepository;
        this.companyInfoRepository = companyInfoRepository;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam("identificator") String identificator, Model model) throws Exception {

        Doi doi = new Doi(identificator);
        model.addAttribute("consignment", consignmentRepository.findOne(doi.getConsignmentId()));
        model.addAttribute("company", companyInfoRepository.findAll().get(0));

        return "index";
    }
}
