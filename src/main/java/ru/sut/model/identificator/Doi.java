package ru.sut.model.identificator;

public class Doi {

    private String identificator;
    private String preffix;
    private String suffix;
    private int consignmentId;
    private int manufactureId;
    private int productId;

    private Doi(){
    }

    public Doi(String identificator) throws Exception {
        this.identificator = identificator;
        init();
    }

    private void init() throws Exception {
        String[] i = identificator.split("/");

        if (i.length != 2) throw new Exception("Invalid identificator!");
        else {
            preffix = i[0];
            suffix = i[1];
        }

        String[] s = suffix.split("\\.");
        if (s.length != 3) throw new Exception("Invalid suffix!");
        else {
            productId = Integer.parseInt(s[0]);
            manufactureId = Integer.parseInt(s[1]);
            consignmentId = Integer.parseInt(s[2]);
        }
    }

    public String getPreffix() {
        return preffix;
    }

    public String getSuffix() {
        return suffix;
    }

    public int getConsignmentId() {
        return consignmentId;
    }

    public int getManufactureId() {
        return manufactureId;
    }

    public int getProductId() {
        return productId;
    }

    public String getIdentificator() {
        return identificator;
    }
}
