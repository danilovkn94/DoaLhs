package ru.sut.model.tables;

import javax.persistence.*;

@Entity
public class Consignment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String manufactureDate;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "MANUFACTURE_ID")
    private Manufacture manufacture;

    public Consignment() {
    }

    public Consignment(String manufactureDate, Product product, Manufacture manufacture) {
        this.manufactureDate = manufactureDate;
        this.product = product;
        this.manufacture = manufacture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Manufacture getManufacture() {
        return manufacture;
    }

    public void setManufacture(Manufacture manufacture) {
        this.manufacture = manufacture;
    }

    public String getSuffix(){
        return product.getId() + "." + manufacture.getId() + "." + id;
    }
}
