package ru.sut.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.tables.CompanyInfo;

public interface CompanyInfoRepository extends JpaRepository<CompanyInfo, Integer>{
}
