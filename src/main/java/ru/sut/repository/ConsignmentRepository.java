package ru.sut.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.tables.Consignment;

public interface ConsignmentRepository extends JpaRepository<Consignment, Integer>{
}
