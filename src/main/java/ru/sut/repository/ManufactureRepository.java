package ru.sut.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.tables.Manufacture;

public interface ManufactureRepository extends JpaRepository<Manufacture, Integer>{
}
