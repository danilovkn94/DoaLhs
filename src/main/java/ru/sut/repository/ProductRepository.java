package ru.sut.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.tables.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{
}
